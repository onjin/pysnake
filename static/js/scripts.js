$(document).ready(function() {
    var moves_history = {};
    var current_step = 0;
    var point_size = 5;
    var interval = false;
    var current_enemy_snake;
    var tournament_result;

    $('#loader-wrapper').hide();

    $('input[type=radio][name=enemy_snake]').change(function() {
        current_enemy_snake = this.value;
        clear_board();
        if (moves_history[current_enemy_snake]) {
            do_move(moves_history[current_enemy_snake].data[0], forward=true);
        }
    });

    var get_board = function(uuid_data, csrf) {
        var board_interval = setInterval(function () {
            $.ajax({
                data: {'uuid': uuid_data.uuid, 'csrfmiddlewaretoken': csrf},
                type: 'POST',
                url: '/get-board/',
                success: function(response) { 
                    if (response.status == "OK") {
                        if (!response.error) {
                            $('#loader-wrapper').hide();
                            data = jQuery.parseJSON(response.moves_history);
                            moves_history[uuid_data.name] = {
                                'data': data
                            };
                            $('#snake_code_errors').text('');
                            $("." + uuid_data.name).addClass('ready')
                            tournament_result = ' Result (' + response.snake1_result + '-' + response.snake2_result + ')';
                            $("#" + uuid_data.name + "_result").text(' Result (' + response.snake1_result + '-' + response.snake2_result + ')');
                        }
                        else {
                            $('#snake_code_errors').text(response.error);
                            $("." + uuid_data.name).addClass('wrong')
                        }
                        clearInterval(board_interval);
                    }
                },
                error: function(e, x, r) {
                    // $('#error_div).html(e); // update the DIV
                }
            });
        }, 5000);
    };

    var strip_number = function(word) {
        numbers = word.substring(1, word.length-1).split(',');
        numbers[0] = parseInt(numbers[0]);
        numbers[1] = parseInt(numbers[1]);
        return numbers;
    };

    var paint_point = function(ctx, point, color) {
        ctx.fillStyle = color;
        if (point) {
            var new_point = strip_number(point);
            ctx.fillRect(new_point[0]*5,new_point[1]*5,point_size,point_size);
        }
    };

    var do_move = function(move, forward){
        var c = document.getElementById("board");
        var ctx = c.getContext("2d");
        
        if (forward) {
            // removing
            paint_point(ctx, move.food.r, "#FFF");
            paint_point(ctx, move.snake1.r, "#FFF");
            paint_point(ctx, move.snake2.r, "#FFF");
            // adding
            paint_point(ctx, move.food.a, "#000");
            paint_point(ctx, move.snake1.a, "#F00");
            paint_point(ctx, move.snake2.a, "#00F");
        }
        else {
            // removing
            paint_point(ctx, move.food.a, "#FFF");
            paint_point(ctx, move.snake1.a, "#FFF");
            paint_point(ctx, move.snake2.a, "#FFF");
            // adding
            paint_point(ctx, move.food.r, "#000");
            paint_point(ctx, move.snake1.r, "#F00");
            paint_point(ctx, move.snake2.r, "#00F");
        }
    };

    $('#step_forward').click(function() {
        if (current_step == (moves_history[current_enemy_snake].data.length-1)) {
            // display error
        }
        else {
            current_step = current_step + 1;
            var current_move = moves_history[current_enemy_snake].data[current_step];
            do_move(current_move, forward=true);
        }        
    });

    $('#step_backward').click(function() {
        if (current_step == 0) {
            // display error
        }
        else {
            var current_move = moves_history[current_enemy_snake].data[current_step];
            do_move(current_move, forward=false);
            current_step = current_step - 1;
        }
    });

    $('#play').click(function() {
        var current_move;
        interval = setInterval(function() {
            if (!moves_history[current_enemy_snake]) {
                clearInterval(interval);
            }
            else if (current_step == (moves_history[current_enemy_snake].data.length-1)) {
                if (tournament_result) {
                    $("#tournament_final_result").text(tournament_result);
                }
                clearInterval(interval);
            }
            else {
                current_step = current_step + 1;
                current_move = moves_history[current_enemy_snake].data[current_step];
                do_move(current_move, forward=true);
            } 

        }, 60);
    });

    $('#pause').click(function() {
        clearInterval(interval);
    });

    var clear_board = function() {
        current_step = 0;
        clearInterval(interval);
        var c = document.getElementById("board");
        var ctx = c.getContext("2d");
        ctx.fillStyle = "#FFF";
        ctx.fillRect(0,0,250,250);
    };

    var initial_clear = function() {
        moves_history = {};
        var radList = document.getElementsByName('enemy_snake');
        for (var i = 0; i < radList.length; i++) {
          if(radList[i].checked) radList[i].checked = false;
        }
        $(".snake_label").removeClass('ready');
        $(".snake_label").removeClass('wrong');
        $('#snake_code_errors').text('');
        $("#randomkonda_result").text('');
        $("#blind_mamba_result").text('');
        $("#cobra_star_result").text('');
        $("#tournament_final_result").text('');
    };

    $('#snake_form').submit(function(e) {
        initial_clear();
        $('#loader-wrapper').show();
        $.ajax({
            data: $(this).serialize(),
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            success: function(response) { 
                if (response.status == "OK") {
                    clear_board();
                    jQuery.each(response.game_uuids, function(index, uuid_data) {
                        get_board(uuid_data, response.csrf);
                    });
                }
                else {
                    if (response.errors.secret_key){
                        $('#secret_key_errors').text(response.errors.secret_key);
                    }
                    else {
                        $('#secret_key_errors').text('');
                    }
                    if (response.errors.snake_code){
                        $('#snake_code_errors').text(response.errors.snake_code);
                    }
                    else {
                        $('#snake_code_errors').text('');
                    }
                }
            },
            error: function(e, x, r) {
                // $('#error_div).html(e); // update the DIV
            }
        });
        e.preventDefault();
        return false;
    });

    $('#snake_tournament_form').submit(function(e) {
        initial_clear();
        var selected = [];
        var selected_names = [];
        $('#snakes input:checked').each(function() {
            selected.push($(this).attr('value'));
            selected_names.push($(this).attr('data-name'));
        });
        if (selected_names.length == 2){
            $('#snake1_name').text(selected_names[0])
            $('#snake2_name').text(selected_names[1])
        }
        $.ajax({
            data: {'snakes': selected, csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value},
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            success: function(response) { 
                if (response.status == "OK") {
                    clear_board();
                    jQuery.each(response.game_uuids, function(index, uuid_data) {
                        get_board(uuid_data, response.csrf);
                    });
                }
                else {
                    if (response.errors.secret_key){
                        $('#secret_key_errors').text(response.errors.secret_key);
                    }
                    else {
                        $('#secret_key_errors').text('');
                    }
                    if (response.errors.snake_code){
                        $('#snake_code_errors').text(response.errors.snake_code);
                    }
                    else {
                        $('#snake_code_errors').text('');
                    }
                }
            },
            error: function(e, x, r) {
                // $('#error_div).html(e); // update the DIV
            }
        });
        e.preventDefault();
        return false;
    });

    $(document).delegate('#id_snake_code', 'keydown', function(e) {
      var keyCode = e.keyCode || e.which;

      if (keyCode == 9) {
        e.preventDefault();
        var start = $(this).get(0).selectionStart;
        var end = $(this).get(0).selectionEnd;

        // set textarea value to: text before caret + tab + text after caret
        $(this).val($(this).val().substring(0, start)
                    + "\t"
                    + $(this).val().substring(end));

        // put caret at right position again
        $(this).get(0).selectionStart =
        $(this).get(0).selectionEnd = start + 1;
      }
    });

    var results_interval = setInterval(function() {
        $.ajax({
            data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value},
            type: 'POST',
            url: '/get-results/',
            success: function(response) { 
                $('#results_table').html(response.results_table)
            },
            error: function(e, x, r) {
                // $('#error_div).html(e); // update the DIV
            }
        });
    }, 10000);

});

