from django.http import HttpResponse
import simplejson

class JsonResponse(HttpResponse):
    """
        JSON response
    """
    def __init__(self, content, content_type='application/json', status=None):
        super(JsonResponse, self).__init__(
            content=simplejson.dumps(content),
            content_type=content_type,
            status=status,
        )