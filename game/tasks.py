from __future__ import absolute_import
import celery
from game.board import Board
from game.models import GameHistory
from teams.models import Team
import json

@celery.task
def calculate_game(code, uuid=None, enemy_snake_name=None, secret_key=None):
	scores = [0, 0, 0]
	enemy_snake = __import__('game.snakes.%s' %enemy_snake_name, globals(), locals(), ['move'], -1)
	error = None
	try:
		board = Board(snake1_move=code, snake1_executable=True, snake2_move=enemy_snake.move, snake2_executable=False)
	except Exception as e:
		error = e

	if not error:
		full_outcome = list(board.turn())
		last_move = full_outcome[-1]
		if not isinstance(last_move, dict):
			error = last_move

	if error:
		game_history = GameHistory(
			uuid=uuid,
			error=str(error)
		)
		game_history.save()
		return

	result = last_move.pop('result')
	game_history = GameHistory(
		uuid=uuid,
		moves_history=json.dumps(full_outcome),
		snake1_result=result['snake1'],
		snake2_result=result['snake2'],
	)
	game_history.save()
	team = Team.objects.get(secret_key=secret_key)

	if result.get('snake1', 0) > team.best_results.get(enemy_snake_name):
		if enemy_snake_name == 'randomkonda':
			team.best_randomkonda_result = result.get('snake1', 0)
		elif enemy_snake_name == 'blind_mamba':
			team.best_blind_mamba_result = result.get('snake1', 0)
		elif enemy_snake_name == 'cobra_star':
			team.best_cobra_star_result = result.get('snake1', 0)

		if enemy_snake_name == 'cobra_star':
			team.best_snake = code
		team.save()
	return

@celery.task
def calculate_tournament_game(snake1, snake2, uuid=None):
	score = 0
	error = None
	try:
		board = Board(snake1_move=snake1, snake1_executable=True, snake2_move=snake2, snake2_executable=True)
	except Exception as e:
		error = e

	if not error:
		full_outcome = list(board.turn())
		last_move = full_outcome[-1]
		if not isinstance(last_move, dict):
			error = last_move

	if error:
		game_history = GameHistory(
			uuid=uuid,
			error=str(error)
		)
		game_history.save()
		return

	result = last_move.pop('result')
	game_history = GameHistory(
		uuid=uuid,
		moves_history=json.dumps(full_outcome),
		snake1_result=result['snake1'],
		snake2_result=result['snake2'],
	)
	game_history.save()

	return