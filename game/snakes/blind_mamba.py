def move(snake1=None, food=None, **kwargs):
	head = snake1.head
	if head.x > food.x:
		return 'l'
	elif head.x < food.x:
		return 'r'
	elif head.y > food.y:
		return 'u'
	elif head.y < food.y:
		return 'd'
